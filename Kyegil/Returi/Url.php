<?php
/**
 * Created by Kyegil
 * Date: 30/07/2020
 * Time: 08:28
 */

namespace Kyegil\Returi;

/**
 * Class Url
 * @package Kyegil\Returi
 */
class Url
{
    /** @var string  */
    public $url;

    /** @var string */
    public $title;

    /** @var \DateTime */
    public $time;

    /**
     * Url constructor.
     * @param $url
     */
    public function __construct($url)
    {
        if(is_string($url)) {
            $this->url = $url;
        }
        if(is_array($url) || is_object($url)) {
            settype($url, 'object');
            $this->url = isset($url->url) ? $url->url : '';
            $this->title = isset($url->title) ? $url->title : '';
        }
        $this->time = date_create();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->url;
    }
}