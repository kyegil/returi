<?php
/**
 * Returi
 *
 * php class to provide on-site browsing history for return / back buttons
 */

namespace Kyegil\Returi;

/**
 * Class returi
 */
class Returi {

    /** @var Url */
    protected $baseUrl;

    /**
     * Returi constructor.
     *
     * @param $baseUrl
     * @param string $baseTitle
     */
    public function __construct(
        $baseUrl,
        $baseTitle = ''
    ) {
        $this->setBaseUrl($baseUrl, $baseTitle);
    }

    /**
     * Set the Base (Home) Url
     *
     * @param string $url The home page url
     * @param string|null $title The page title or description
     * @return $this
     */
    public function setBaseUrl(string $url, string $title = null) {
        if(!is_a($url, Url::class)) {
            $url = new Url(['url' => $url, 'title' => $title]);
        }
        $this->baseUrl = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function determineUrl() {
        $result = (
            isset($_SERVER["HTTPS"]) && ($_SERVER["HTTPS"] == "on")
            ? "https://"
            : "http://"
        )
        . urldecode($_SERVER['SERVER_NAME'])
        . (
            ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
            ? ":{$_SERVER['SERVER_PORT']}"
            : ""
        )
        . urldecode($_SERVER['REQUEST_URI']);
        return $result;
    }

    /**
     * Set the current page
     *
     * @param string $url
     * @param string $title
     * @return $this
     */
    public function set($url = null, $title = null) {
        if(!$url) {
            $url = $this->determineUrl();
        }
        $baseUrl = $this->getBaseUrl();
        $url = new Url(['url' => $url, 'title' => $title]);
        if(strval($url) == strval($baseUrl)) {
            $this->reset();
        }
        else {
            if(isset($_SESSION['returi']) && is_array($_SESSION['returi'])) {
                $newReturi = [$baseUrl];
                foreach($_SESSION['returi'] as $past) {
                    if(strval($past) != strval($url)) {
                        if(strval($past) != strval($baseUrl)) {
                            $newReturi[] = $past;
                        }
                    }
                    else {
                        break;
                    }
                }
                $_SESSION['returi'] = $newReturi;
            }
        }

        $_SESSION['returi'][] = $url;
        return $this;
    }

    /**
     * @return $this
     */
    public function reset() {
        $_SESSION['returi'] = [];
        return $this;
    }

    /**
     * @param int $skip How many addresses in the trace should be skipped
     * @param string $url
     * @return Url
     */
    public function get($skip = 0, $url = "") {
        if(!$url) {
            $url = $this->determineUrl();
        }
        $previous = $this->baseUrl;
        $trace = $this->getReverseTrace();
        foreach($trace as $past) {
            if(strval($past) != strval($url)) {
                if($skip < 1) {
                    $previous = $past;
                    break;
                }
                $skip--;
            }
        }
        return $previous;
    }

    /**
     * @return Url[]
     */
    public function getTrace(): array
    {
        if(isset($_SESSION['returi']) && is_array($_SESSION['returi'])) {
            return $_SESSION['returi'];
        }
        else return [];
    }

    /**
     * @return Url[]
     */
    public function getReverseTrace(): array
    {
        $trace = $this->getTrace();
        return array_reverse($trace);
    }

    /**
     * @return Url
     */
    public function getBaseUrl() {
        return $this->baseUrl;
    }
}